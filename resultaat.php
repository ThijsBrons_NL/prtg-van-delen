<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta http-equiv="refresh" content="60;url=./network/ping.php" />
    <link rel="stylesheet" href="./style/style.css">
</head>

<body>
    <table id="paginate">
        <tr>
            <th>Bedrijfsnaam:</th>
            <th>locatie:</th>
            <th>IP:</th>
            <th>Status:</th>
        </tr>
        <?php

        // Start Formulier naar database

        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "monitor";
        $conn = new mysqli($servername, $username, $password, $database);

        // Start Controleer verbinding

        if ($conn->connect_error) {
            die("Verbinding mislukt: " . $conn->connect_error);
        }

        // End Controleer verbinding

        // Start Input DB

        if (isset($_GET['page_no']) && $_GET['page_no'] != "") {
            $page_no = $_GET['page_no'];
        } else {
            $page_no = 1;
        }

        $total_records_per_page = 12;

        $offset = ($page_no - 1) * $total_records_per_page;
        $previous_page = $page_no - 1;
        $next_page = $page_no + 1;
        $adjacents = "2";

        $result_count = mysqli_query(
            $conn,
            "SELECT COUNT(*) As total_records FROM `klant gegevens`"
        );
        $total_records = mysqli_fetch_array($result_count);
        $total_records = $total_records['total_records'];
        $total_no_of_pages = ceil($total_records / $total_records_per_page);
        $second_last = $total_no_of_pages - 1; // total pages minus 1
        $result = mysqli_query(
            $conn,
            "SELECT * FROM `klant gegevens`  ORDER BY `klant gegevens`.`bedrijfsnaam` ASC LIMIT $offset, $total_records_per_page"
        );
        while ($row = mysqli_fetch_array($result)) {
            $ip = $row['ip'];
            $bedrijfsnaam = $row['bedrijfsnaam'];
            $locatie = $row['locatie'];
            echo "<tr> 
                            <td>" . $row['bedrijfsnaam'] . "</td> 
                            <td>" . $row['locatie'] . "</td> 
                            <td>" . "" . " 
                            <div class='dropdown'>
  <button class='dropbtn'>$ip</button>
  <div class='dropdown-content'>
  <a target='blank' href='https://$ip'>Router configuratie</a>
  <a href='./network/portscan.php?ip=$ip'>Poort scan</a>
  <a href='./network/trace.php?ip=$ip'>Trace</a>
  <a href='./network/ssh.php?ip=$ip'>SSH</a>
  </div>
</div>
                            </td>
                            <td>";
            $status = $row['status'];

            foreach ((array) $status as $status) {
                if ($status == "up") {
                    echo $row['status'];
                } else {
                    $myAudioFile = "./alarm.wav";
                    echo '<audio autoplay="true" style="display:none;">
         <source src="' . $myAudioFile . '" type="audio/wav">
      </audio>';
                    file_get_contents("https://api.telegram.org/bot1947137268:AAETY0dzSpwT1cdUIKNScZ2Yfrf63S7PutY/sendMessage?chat_id=@prtgteamvandelen&text=Hallo, $bedrijfsnaam ligt eruit! Met het IP adres: $ip, en op locatie: $locatie.");
                    echo $row['status'];
                }
            }

            echo "</td>

                         </tr>";
        }

        // End Input DB

        $conn->close();

        // End Formulier naar database

        ?>
    </table>
    <a href="./forms/form_aanmaken.html">Aanmaken</a>
    <a href="./forms/form_verwijderen.html">Verwijderen</a>
    <a href="./forms/form_wijzigen.html">Wijzigen</a>

    <div style='padding: 10px 20px 0px; border-top: dotted 1px #CCC;'>
        <strong>Pagina <?php echo $page_no . " van " . $total_no_of_pages; ?></strong>
    </div>
    <ul class="pagination">
        <?php if ($page_no > 1) {
            echo "<li><a href='?page_no=1'>Eerste pagina</a></li>";
        } ?>

        <li <?php if ($page_no <= 1) {
                echo "class='disabled'";
            } ?>>
            <a <?php if ($page_no > 1) {
                    echo "href='?page_no=$previous_page'";
                } ?>>Terug</a>
        </li>

        <li <?php if ($page_no >= $total_no_of_pages) {
                echo "class='disabled'";
            } ?>>
            <a <?php if ($page_no < $total_no_of_pages) {
                    echo "href='?page_no=$next_page'";
                } ?>>Volgende</a>
        </li>

        <?php if ($page_no < $total_no_of_pages) {
            echo "<li><a href='?page_no=$total_no_of_pages'>Laatste pagina &rsaquo;&rsaquo;</a></li>";
        } ?>
        <br><br>
        <li><a href="./index.php">HOME PAGINA</a></li>
    </ul>
</body>

</html>